	###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 09a - Cat Empire
#
# @file    Makefile
# @version 1.0
#
# @author Jeraldine Milla <millaje@hawaii.edu>
# @brief  Lab 09a - Cat Empire - EE 205 - Spr 2021
# @date   20 Apr 2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

AR       = ar

TARGETS  = familyTree catList catBegat catGenerations test

all: $(TARGETS)

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

libcat.a: cat.o
	$(AR) -rsv $@ $^

$(TARGETS): %: %.cpp libcat.a
	$(CXX) $(CXXFLAGS) -o $@ -L. $< -lcat

clean:
	rm -f *.o *.a $(TARGETS)
