//random test
//
//
///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.hpp
/// @version 1.0
///
/// Testing cats
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   20 Apr 2021
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include "cat.hpp"

#define CAT_NAMES_FILE "names.txt"


using namespace std;
const int NUMBER_OF_CATS = 20;

int main(){
   cout<<"Hello!"<<endl;

   cout << "Welcome to Cat Empire!" << endl;

   Cat::initNames();

    CatEmpire catEmpire;

   for( int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
      Cat* newCat = Cat::makeCat();

       catEmpire.addCat( newCat );
   }

   cout << "Print a family tree of " << NUMBER_OF_CATS << " cats" << endl;



}


